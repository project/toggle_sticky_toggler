<?php

namespace Drupal\toggle_sticky_toggler\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Toggle Sticky Toggler controller.
 */
class ToggleStickyTogglerController extends ControllerBase {

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs a new CKEditor5MediaController.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(RequestStack $request_stack) {
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack')
    );
  }

  /**
   * Toggle a node's sticky status.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The requested node object.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect response back to URL where the link was clicked.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function toggleStickyStatus(NodeInterface $node) {
    $node->setSticky(!$node->isSticky());
    $node->save();

    $http_referer = $this->requestStack->getCurrentRequest()->server->get('HTTP_REFERER');
    $redirectUrl = Url::fromUri($http_referer, ['absolute' => TRUE])->getUri();
    return new RedirectResponse($redirectUrl);
  }

}
